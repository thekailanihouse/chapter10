import { Box, Container, Flex, Stack, Text } from '@chakra-ui/react'
import { CloseIcon, HamburgerIcon } from '@chakra-ui/icons'
import Link from 'next/link'
import React, { useEffect, useState } from 'react'

const NavbarContainer = ({ children }) => {
    const [scroll, setScroll] = useState(false)
    const listenScroll = () => {
        if(window.scrollY >= 75){
            setScroll(true)
        }else{
            setScroll(false)
        }
    }

    useEffect(() => {
      window.addEventListener('scroll', listenScroll,true)
      return () => {
        window.removeEventListener('scroll', listenScroll)
      }
    }, [])
    
    return (
        <Flex
            position={'fixed'}
            zIndex={2}
            w={'100%'}
            h={'75px'}
            align={'center'}
            bgColor={scroll ? 'white' : 'transparent'}
            boxShadow={scroll ? 'md' : 'none'}
        >
            <Container maxW={'container.xl'}>
                <Flex
                    justify={'space-between'}
                    align={'center'}
                    flexDirection={'row'}>
                    {children}
                </Flex>
            </Container>
        </Flex>
    )
}

const Logo = () => {
    return (
        <Box>
            <Text fontWeight={"bold"} fontSize={["1xl", "4xl"]}>Logo</Text>
        </Box>
    )
}

const MenuItems = ({ children, to = '/' }) => {
    return (
        <Box>
            <Link href={to}>
                {children}
            </Link>
        </Box>
    )
}

const MenuLinks = () => {
    return (
        <Box
            display={['none', 'none', 'block', 'block']}
        >
            <Stack
                spacing={8}
                direction={['column', 'row']}
                align={'center'}
            >
                <MenuItems>Home</MenuItems>
                <MenuItems to='/game'>Game</MenuItems>
                <MenuItems to='/login'>Login/register</MenuItems>
            </Stack>
        </Box>
    )
}

const ToggleMenu = ({ toggle, isOpen }) => {
    return (
        <Box onClick={toggle} display={{ md: 'none' }}>
            {isOpen ? <CloseIcon fontSize={'1xl'} /> : <HamburgerIcon fontSize={'2xl'} />}
        </Box>
    )
}

const MobileLinks = ({ isOpen }) => {
    return (
        <Flex
            display={{ base: isOpen ? 'show' : 'none' }}
            position={'fixed'}
            zIndex={1}
            bg={'gray.100'}
            w={'100%'}
        >
            <Stack
                spacing={8}
                direction={['column', 'row']}
                justifyContent={'center'}
                alignItems={'center'}
                h={'100vh'}
            >
                <MenuItems>Home</MenuItems>
                <MenuItems to='/game'>Game</MenuItems>
                <MenuItems to='/login'>Login/register</MenuItems>
            </Stack>
        </Flex>
    )
}

const Header = () => {
    const [isOpen, setIsOpen] = useState(false)
    const toggle = () => setIsOpen(!isOpen)
  return (
    <>
    <NavbarContainer maxWidth="container.xl">
        <Logo />
        <MenuLinks />
        <ToggleMenu toggle={toggle} isOpen={isOpen} />
    </NavbarContainer>
    <MobileLinks isOpen={isOpen}/>
    </>
  )
}

export default Header