import { Box, Container, Flex, Image } from '@chakra-ui/react'
import React from 'react'

const Footer = () => {
    return (
        <Container maxW="container.xl">
        <Flex justify="center" align="center" direction="row">
            <a
                href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
                target="_blank"
                rel="noopener noreferrer"
            >
                Powered by{' '}
                <span>
                    <Image src="/vercel.svg" alt="Vercel Logo" boxSize='100px' />
                </span>
            </a>
        </Flex>
        </Container>
    )
}

export default Footer