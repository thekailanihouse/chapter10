import React from 'react'
import Footer from './layout/Footer'
import Header from './layout/Header'

const Layout = ({children}) => {
  return (
    <>
    <Header />
    <main>{children}</main>
    <Footer />
    </>
  )
}

export default Layout